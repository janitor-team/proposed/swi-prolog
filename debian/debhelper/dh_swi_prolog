#! /usr/bin/perl

=head1 NAME

dh_swi_prolog - calculates SWI Prolog packages dependencies

=cut

use strict;
use Debian::Debhelper::Dh_Lib;

=head1 SYNOPSYS

B<dh_swi_prolog> [S<I<debhelper options>>]

=head1 DESCRIPTION

dh_swi_prolog is a helper program that adds swi-prolog:Depends substitution
variable that contains current SWI Prolog VM virtual package name. This
variable should be used by packages that contain compiled SWI prolog
code to specify dependency on the VM. The virtual package name will change
if there are noncompatible changes in the VM, so binary packages will need
to be rebuilt with a new version of swi-prolog. The variable is there to
detect such breackage.

=cut

init();

my $swi_prolog_vm = "swi-prolog-vm-3";

foreach my $package (@{$dh{DOPACKAGES}}) {
	addsubstvar($package, "swi-prolog:Depends", $swi_prolog_vm);
}

=head1 SEE ALSO

L<debhelper(7)>

=head1 AUTHORS

Eugeniy Meshcheryakov <eugen@debian.org>

=cut
